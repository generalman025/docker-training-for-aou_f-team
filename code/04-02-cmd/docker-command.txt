
// Create network
docker network create my-network-2

// Create MongoDB container
docker run --name mongodb -v data:/data/db -d --rm -p 27017:27017 --network my-network-2 -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=secret mongo

// Build backend image
docker build -t my-backend:latest .

// Create backend container
docker run --name my-backend -d --rm --network my-network-2 -p 80:80 my-backend:latest

// Build frontend application
docker build -t my-frontend:latest .

// Create frontend container
docker run --name my-frontend -d --rm -it --network my-network-2 -p 3000:3000 my-frontend:latest