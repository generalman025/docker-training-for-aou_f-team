
// Create network
docker network create my-network

// Create MongoDB container
docker run --name my-mongodb -d --rm -p 27017:27017 --network my-network mongo

// Build node application
docker build -t my-img:latest .

// Create node application container
docker run --name my-node -d --rm --network my-network -p 3000:3000 my-img:latest